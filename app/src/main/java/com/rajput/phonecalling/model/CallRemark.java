package com.rajput.phonecalling.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class CallRemark {

    public String rid;
    public String number;
    public String remark;
    public Map<String, Boolean> stars = new HashMap<>();

    public CallRemark() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public CallRemark(String rid, String number, String remark) {
        this.rid = rid;
        this.number = number;
        this.remark = remark;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("rid", rid);
        result.put("number", number);
        result.put("remark", remark);

        return result;
    }
    // [END post_to_map]

}