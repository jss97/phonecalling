package com.rajput.phonecalling.listener;

public interface IncomingListener {
 /**
  * To call this method when new message received and send back 
  * @param message Message
 */
 void onCallDisconnect(boolean isDisconnected);
}